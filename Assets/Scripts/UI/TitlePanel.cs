﻿using UnityEngine;

public class TitlePanel : MonoBehaviour
{
    private AudioSource bgmSource = null;
    private bool IsLoading = false;

    private void Awake()
    {
        bgmSource = GameObject.Find( "BGMSource" ).transform.GetComponent<AudioSource>();
    }

    private void Start()
    {
        DontDestroyOnLoad( bgmSource );
    }

    private void Update()
    {
        if ( Input.GetKeyDown( KeyCode.Space ) && IsLoading == false )
        {
            IsLoading = true;
            GameStateManager.Instance().LoadLevel( 1 );
        }
    }
}
