﻿using UnityEngine;
using UnityEngine.UI;

public class GameStateUI : MonoBehaviour
{
    private Text GameStateText = null;

    private void Awake()
    {
        GameStateText = GetComponent<Text>();
    }

    private void Start()
    {
        GameStateManager.Instance().UpdateGameState += UpdateGameState;
    }

    private void OnDestroy()
    {
        GameStateManager.Instance().UpdateGameState -= UpdateGameState;
    }

    private void UpdateGameState( int level, int upsetCounter, int upsetGoal )
    {
        GameStateText.text = "Level: " + level + "\n";
        GameStateText.text += "Cut: " + upsetCounter + " / " + upsetGoal;
    }
}
