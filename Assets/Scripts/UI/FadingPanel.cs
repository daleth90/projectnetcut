﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadingPanel : MonoBehaviour
{
    public delegate void AfterFadeDelegate();
    private Image BlackImage = null;
    public bool sceneStarting = true;

    private void Awake()
    {
        BlackImage = transform.GetComponentInChildren<Image>();
    }

    private void Start()
    {
        GameStateManager.Instance().SetFadingPanel( this );
    }

    private void Update()
    {
        if ( sceneStarting )
            StartScene();
    }

    private void StartScene()
    {
        FadeToClear();

        if ( BlackImage.color.a <= 0.05f )
        {
            BlackImage.color = Color.clear;
            BlackImage.enabled = false;

            sceneStarting = false;
        }
    }

    public void EndScene( string SceneName, bool DeathFlag = false )
    {
        BlackImage.enabled = true;
        if ( DeathFlag == true )
            FadeFromDeathToBlack( () => { Application.LoadLevel( SceneName ); } );
        else
            FadeToBlack( () => { Application.LoadLevel( SceneName ); } );
    }

    private void FadeToClear()
    {
        StartCoroutine( LerpBackgroundColor( BlackImage.color, Color.clear ) );
    }

    private void FadeToBlack( AfterFadeDelegate function = null )
    {
        StartCoroutine( LerpBackgroundColor( BlackImage.color, Color.black, function ) );
    }

    private void FadeFromDeathToBlack( AfterFadeDelegate function = null )
    {
        StartCoroutine( LerpBackgroundColor( new Color( 1f, 0, 0, 0.5f ), Color.black, function ) );
    }

    private IEnumerator LerpBackgroundColor( Color start, Color end, AfterFadeDelegate function = null )
    {
        for ( float t = 0; t <= 1.0f; t += Time.deltaTime )
        {
            BlackImage.color = Color.Lerp( start, end, t );
            yield return null;
        }

        if ( function != null )
            function();
    }
}