﻿using UnityEngine;
using System.Collections;

public class ThanksPanel : MonoBehaviour
{
    [SerializeField]private float StayTime = 5.0f;
    private float TimeCounter = 0f;
    private AudioSource bgmSource = null;

    private void Awake()
    {
        bgmSource = GameObject.Find( "BGMSource" ).transform.GetComponent<AudioSource>();
    }

    private void Start()
    {
        Destroy( bgmSource );
    }

    private void Update()
    {
        TimeCounter += Time.deltaTime;
        if ( TimeCounter >= StayTime )
        {
            GameStateManager.Instance().LoadTitleScene();
        }
    }
}
