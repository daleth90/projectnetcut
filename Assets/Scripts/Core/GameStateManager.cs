﻿using UnityEngine;

public class GameStateManager
{
    public delegate void UpdateGameStateEvent( int level, int upsetCounter, int upsetGoal );
    public delegate void LevelClearEvent();
    public UpdateGameStateEvent UpdateGameState;
    public LevelClearEvent LevelClear;

    private const int MAX_LEVEL = 5;
    private int[] EachLevelGoal = new int[ 5 ] { 3, 4, 3, 4, 5 };

    public enum GameState { PLAYING, CHANGING }
    private int Level = 1;
    private int UpsetEnemyCounter = 0;
    private int UpsetEnemyLevelGoal = 3;
    public GameState gameState = GameState.PLAYING;
    private FadingPanel theFadingPanel = null;

    static private GameStateManager instance = null;

    static public GameStateManager Instance()
    {
        if ( instance == null )
        {
            instance = new GameStateManager();
        }

        return instance;
    }

    public void AddUpsetEnemy()
    {
        ++UpsetEnemyCounter;
        if ( UpdateGameState != null )
            UpdateGameState( Level, UpsetEnemyCounter, UpsetEnemyLevelGoal );

        if ( UpsetEnemyCounter == UpsetEnemyLevelGoal )
        {
            if ( Level == MAX_LEVEL )
                LoadThanksScene();
            else
                LoadLevel( ++Level );
        }
    }

    public void ReduceUpsetEnemy()
    {
        --UpsetEnemyCounter;
        if ( UpdateGameState != null )
            UpdateGameState( Level, UpsetEnemyCounter, UpsetEnemyLevelGoal );
    }

    private void ResetGameState( int level )
    {
        Level = level;
        UpsetEnemyCounter = 0;
        UpsetEnemyLevelGoal = EachLevelGoal[ level - 1 ];
    }

    public void SetFadingPanel( FadingPanel theFadingPanel )
    {
        this.theFadingPanel = theFadingPanel;

        ResetGameState( Level );
        gameState = GameState.PLAYING;

        if ( UpdateGameState != null )
            UpdateGameState( Level, UpsetEnemyCounter, UpsetEnemyLevelGoal );
    }

    public void LoadLevel( int level )
    {
        gameState = GameState.CHANGING;
        ResetGameState( level );
        theFadingPanel.EndScene( "Level_" + level );
    }

    public void ReloadLevel()
    {
        if ( gameState == GameState.CHANGING ) return;
        gameState = GameState.CHANGING;
        ResetGameState( Level );
        theFadingPanel.EndScene( "Level_" + Level, true );
    }

    public void LoadThanksScene()
    {
        gameState = GameState.CHANGING;
        theFadingPanel.EndScene( "Thanks" );
    }

    public void LoadTitleScene()
    {
        gameState = GameState.CHANGING;
        theFadingPanel.EndScene( "Title" );
    }
}
