﻿using UnityEngine;

public class Netcutter : MonoBehaviour
{
    private void OnTriggerEnter2D( Collider2D other )
    {
        if ( other.tag == "Enemy" )
        {
            other.GetComponent<Enemy>().EnterCutRange();
        }
    }

    private void OnTriggerExit2D( Collider2D other )
    {
        if ( other.tag == "Enemy" )
        {
            other.GetComponent<Enemy>().ExitCutRange();
        }
    }
}
