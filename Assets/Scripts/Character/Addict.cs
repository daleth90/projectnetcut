﻿using UnityEngine;

public class Addict : Enemy
{
    public Player player = null;

    private float ShockTimeCounter = 0;
    private const float SHOCK_TIME = 1.0f;

    private float AngerTimeCounter = 0;
    private const float ANGER_TIME = 5.0f;

    private void Start()
    {
        animator = transform.GetComponent<Animator>();
        rigid2d = transform.GetComponent<Rigidbody2D>();
        audioSource = transform.GetComponentInChildren<AudioSource>();
    }

    private void Update()
    {
        if ( state == State.NORMAL )
            RandomMove();
        else if ( state == State.SHOCK )
            ShockBehavior();
        else if ( state == State.ANGER )
            AngerBehavior();

        if ( NeedToFlip() )
            Flip();
    }

    private void ShockBehavior()
    {
        ShockTimeCounter += Time.deltaTime;
        if ( ShockTimeCounter >= SHOCK_TIME )
        {
            state = State.ANGER;
            animator.SetInteger( "State", (int)state );
            ShockTimeCounter = 0f;
        }
    }

    private void AngerBehavior()
    {
        AngerTimeCounter += Time.deltaTime;
        if ( AngerTimeCounter >= ANGER_TIME )
        {
            state = State.UPSET;
            Move( 0, 0 );
            audioSource.Play();
            animator.SetInteger( "State", (int)state );
            GameStateManager.Instance().AddUpsetEnemy();
            AngerTimeCounter = 0f;
        }
        else
        {
            TargetPlayer();
        }
    }

    public override void EnterCutRange()
    {
        state = State.SHOCK;
        Move( 0, 0 );
        animator.SetInteger( "State", (int)state );
    }

    private void TargetPlayer()
    {
        if ( transform.localPosition.x - player.transform.localPosition.x > 0 )
            Move( -0.5f, 0 );
        else
            Move( 0.5f, 0 );
    }
}
