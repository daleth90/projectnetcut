﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    protected Transform sprite = null;
    [SerializeField]
    protected float speed = 5.0f;
    protected enum State { NORMAL, UPSET, SHOCK, ANGER }
    protected State state = State.NORMAL;
    protected Rigidbody2D rigid2d = null;
    protected bool IsFacingRight = true;
    protected float StayingTime = 0f;
    protected Animator animator = null;
    protected AudioSource audioSource = null;

    private void Start()
    {
        animator = transform.GetComponent<Animator>();
        rigid2d = transform.GetComponent<Rigidbody2D>();
        audioSource = transform.GetComponentInChildren<AudioSource>();
    }

    private void Update()
    {
        if ( state == State.NORMAL )
            RandomMove();

        if ( NeedToFlip() )
            Flip();
    }

    protected void RandomMove()
    {
        StayingTime += Time.deltaTime;
        if ( StayingTime > 2f )
        {
            Move( Random.Range( -1f, 1f ), 0 );
            StayingTime = 0f;
        }
        else if ( StayingTime > 0.2f )
        {
            Move( 0, 0 );
        }
    }

    protected void Move( float MoveX, float MoveY )
    {
        rigid2d.velocity = new Vector2( MoveX * speed, MoveY * speed );
    }

    protected bool NeedToFlip()
    {
        if ( IsFacingRight == true && rigid2d.velocity.x < 0 )
            return true;
        else if ( IsFacingRight == false && rigid2d.velocity.x > 0 )
            return true;
        else
            return false;
    }

    protected void Flip()
    {
        IsFacingRight = !IsFacingRight;
        sprite.localScale = new Vector3( -sprite.localScale.x, 1, 1 );
    }

    public virtual void EnterCutRange()
    {
        state = State.UPSET;
        Move( 0, 0 );
        audioSource.Play();
        animator.SetInteger( "State", (int)state );
        GameStateManager.Instance().AddUpsetEnemy();
    }

    public void ExitCutRange()
    {
        if ( state == State.UPSET )
            GameStateManager.Instance().ReduceUpsetEnemy();

        state = State.NORMAL;
        animator.SetInteger( "State", (int)state );
    }

    private void OnTriggerStay2D( Collider2D other )
    {
        if ( state == State.ANGER && other.tag == "Player" )
        {
            GameStateManager.Instance().ReloadLevel();
        }
    }
}
