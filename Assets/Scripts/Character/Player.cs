﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]private Transform sprite = null;
    [SerializeField]private float speed = 5.0f;
    [SerializeField]private Netcutter theNetCutter = null;
    private Rigidbody2D rigid2d = null;
    private bool IsFacingRight = true;
    private int CutterSize = 1;

    private void Awake()
    {
        rigid2d = transform.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if ( IsFacingRight == true && rigid2d.velocity.x < 0 )
            Flip();
        else if ( IsFacingRight == false && rigid2d.velocity.x > 0 )
            Flip();
    }

    public void Move( float MoveX, float MoveY )
    {
        rigid2d.velocity = new Vector2( MoveX * speed, MoveY * speed );
    }

    private void Flip()
    {
        IsFacingRight = !IsFacingRight;
        sprite.localScale = new Vector3( -sprite.localScale.x, 1, 1 );
    }

    public void MagnifyNetcutter()
    {
        if ( CutterSize != 5 )
        {
            ++CutterSize;
            theNetCutter.transform.localScale = new Vector3( CutterSize * 8f, CutterSize * 8f, 1 );
        }
    }

    public void ReduceNetcutter()
    {
        if ( CutterSize != 1 )
        {
            --CutterSize;
            theNetCutter.transform.localScale = new Vector3( CutterSize * 8f, CutterSize * 8f, 1 );
        }
    }
}
