﻿using UnityEngine;

public class UserControl : MonoBehaviour
{
    private Player Player = null;

    private void Awake()
    {
        Player = GetComponent<Player>();
    }

    private void Update()
    {
        if ( Input.GetKeyDown( KeyCode.Z ) )
            Player.MagnifyNetcutter();
        else if ( Input.GetKeyDown( KeyCode.X ) )
            Player.ReduceNetcutter();
    }

    private void FixedUpdate()
    {
        if ( GameStateManager.Instance().gameState == GameStateManager.GameState.PLAYING )
        {
            float MoveX = Input.GetAxis( "Horizontal" );
            float MoveY = 0;

            Player.Move( MoveX, MoveY );
        }
        else
            Player.Move( 0, 0 );
    }
}
